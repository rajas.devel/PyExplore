"""
    The MIT License (MIT)

    Copyright (c) 2015 Rajas Abhyankar

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

"""

import inspect
import json
import re
from pip import pip
import importlib
from PyTypeConverter import *


class PyExplore:

    def __init__(self):
        # TODO: Proper initializations for non-static workflow
        pass

    @staticmethod
    def get_modules(as_json=False):
        """
        Returns list of installed python eggs from PIP
        :param as_json: Returns output encoded to JSON String
        :return: List or JSON string containing object/dictionary with package name and it's version
        """
        package_list = [{'name': pkg.key, 'version': pkg.version} for pkg in pip.get_installed_distributions()]
        return json.dumps(package_list) if as_json else package_list

    @staticmethod
    def get_module_members(module_name, as_json=False):
        """
        Returns members of module matching the predicate
        :param module_name: Name of module
        :param as_json: Returns output encoded to JSON String
        :return: Members of module if found otherwise False
        """
        mod = importlib.import_module(module_name)

        # x[0][1].__name__

        # x[0][1].__name__

        criterions = [
            ('modules', inspect.ismodule),
            ('classes', inspect.isclass),
            ('functions', inspect.isfunction),
            ('methods', inspect.ismethod)
        ]

        members = {}

        for c in criterions:
            results = inspect.getmembers(mod, c[1])
            if not results:
                continue

            processed = []

            for result in results:
                if result and result[0] and result[1]:
                    processed.append({
                        'name': result[0],
                        'full_name': result[1].__name__
                    })

            members[c[0]] = processed

        return json.dumps(members) if as_json else members

    @staticmethod
    def get_object_methods(obj,
                           as_json=False,
                           get_private=False,
                           get_docs=True,
                           get_source=False,
                           get_file=False,
                           get_guessed_module=False):
        """
        Returns list of method names of an object with their arguments, documentation, source
        :param obj: Object to be inspected
        :param get_private: Boolean indicating weather methods starting with two underscores should be returned
        :param get_docs: Boolean indicating weather documentation and comments are returned
        :param get_source: Boolean indicating weather source code is returned
        :return dict containing method
        :raises IOError
        """
        methods = inspect.getmembers(obj, predicate=inspect.ismethod)
        info_list = []
        for method in methods:
            # Skip private methods if get_private argument is set to false
            if not get_private and method[0].startswith("__"):
                continue

            # Get function reference
            func = getattr(obj, method[0])
            args, varargs, keywords, defaults = inspect.getargspec(func)

            # If default arguments exist, map default values to their respective arguments
            if defaults is None:
                # None represents no default value set for the argument
                arguments = zip(args[1:], [None] * len(args[1:]))
            else:
                # Map arguments without default to None value
                without_defaults = zip(args[1:-len(defaults)], [None] * len(args[1:-len(defaults)]))
                # Map arguments without default to their respective default values
                with_defaults = zip(args[-len(defaults):], defaults)
                arguments = with_defaults + without_defaults

            info = {
                'method': method[0],
                'arguments': dict(arguments)
            }

            if get_docs:
                doc = inspect.getdoc(method)
                info['doc'] = inspect.cleandoc(doc)
                info['comments'] = inspect.getcomments(obj)

            if get_source:
                info['source'] = inspect.getsource(method)

            if get_file:
                info['file'] = inspect.getfile(method)

            if get_guessed_module:
                info['module'] = inspect.getmodule(method)

            info_list.append(info)

        return info_list if not as_json else json.dumps(info_list)

    @staticmethod
    def execute_object_method_from_dict(requested_method, is_request_json):
        """
        Executes method from dictionary containing object name, method name, arguments
        :param requested_method:
        :param is_request_json: If true output
        :return Result returned by executed function
        :raises KeyError
        """
        if is_request_json:
            requested_method = json.loads(requested_method)

        object_name = requested_method['obj']
        method_name = requested_method['method']
        args = requested_method['args']

        if args is None or len(args) == 0:
            ret = PyExplore.execute_object_method(object_name, method_name, None)
        else:
            ret = PyExplore.execute_object_method(object_name, method_name, **args)
        return ret if not is_request_json else json.dumps(ret)

    @staticmethod
    def execute_object_method(obj, method_name, method_args=None, type_conversions=[]):
        """
        Execute object from arguments passed
        :param obj: Object to which method belongs
        :param method_name: Name of method
        :param method_args: Arguments for method
        :param type_conversions: List of conversions to be performed
        :return Result returned by executed function
        :raises KeyError, NameError
        """
        arguments = {}

        if type_conversions is not None and len(type_conversions) > 0 and method_args is not None and len(method_args) > 0:
            for conversion in type_conversions:
                # Check if conversion method exists and is callable
                try:
                    conversion_method = locals()[conversion['source'] + '_to_' + conversion['destination']]
                    if conversion_method is not None and callable(conversion_method):
                        arguments = {k: conversion_method(v) for (k, v) in method_args.items()}
                except KeyError as ke:
                    continue

        # TODO: Add transformations that do basic transformation such as split, trim etc.
        # if transformations is not None and len(transformations) > 0:
        #     for transformation in type_conversions:
        #         pass

        arguments = arguments if arguments else method_args

        func = getattr(obj, method_name)
        if arguments:
            return func(**method_args)
        else:
            return func()

    @staticmethod
    def exec_function(function_name, args=None):
        """
        Tries to get function in local symbol table and if not found in global symbol table.
        Executes it and returns the results.
        :param function_name: Name of function to be executed
        :param args: Arguments as dictionary, this dictionary will be unpacked and passed as argument
        :return: False if method not found, Otherwise results of of execution of the method
        """
        func = None
        if function_name is not None and len(function_name) > 0:
            try:
                func = locals()[function_name]
            except (KeyError, NameError) as ke:
                try:
                    func = globals()[function_name]
                except (KeyError, NameError) as ke:
                    pass

        try:
            if func:
                return func(**args) if args else func()
        except NameError as ne:
            pass

        return False

# TODO: Add methods of inspect to this
(function () {

    'use strict';

    var app = angular.module('PyExplore', [

    ]).config(['$interpolateProvider', function($interpolateProvider) {
        $interpolateProvider.startSymbol('{[');
        $interpolateProvider.endSymbol(']}');
    }]);

    app.service('ModulesService', ['$http', '$q', function($http, $q){

        function getGlobalModules() {
            return $http.get("/api/modules");
        }

        function getMembers(module) {
            return $http.get('/api/module/' + module + '/members');
        }

        return {
            'getGlobal': getGlobalModules,
            'getMembers': getMembers
        };
    }]);

    app.controller('MainController', ['$scope', '$log', 'ModulesService', function($scope, $log, mods){
        $scope.globalModules = {};
        $scope.selectedModule = {'name': 'Select Module'};
        $log.log('Main controller');
        mods.getGlobal().success(function(response){
            $log.log('Response');
            $scope.globalModules = response;
            $log.log($scope.globalModules);
        }).error(function(response){
            $log.log('Failed');
            $log.log(response);
        });

        $scope.getMembers = function(selectedModule) {
            console.log('Getting submodules');
            console.log(selectedModule);
            mods.getMembers(selectedModule.name).success(function(response){
                $log.log('Got members');
                $log.log(response);
                if(typeof response !== "undefined" && response) {
                    for(var k in response) {
                        selectedModule[k] = response[k];
                    }
                }
                $log.log($scope.globalModules);
                $log.log($scope.selectedModule);
            }).error(function(response){
                $log.log('Error while getting members');
                $log.log(response);
            });
        }


    }]);
    console.log(app);
}());
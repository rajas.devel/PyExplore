"""
    The MIT License (MIT)

    Copyright (c) 2015 Rajas Abhyankar

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

"""


def json_to_object(source):
    pass


def json_to_array(source):
    return json_to_object(source)


def unicode_to_ascii(source):
    """
    Converts unicode to ascii
    :param source: Unicode string
    :return: ASCII string
    """
    return source.encode('ascii', 'ignore') if (isinstance(source, unicode) and source is not None) else source


def ascii_to_unicode(source):
    """
    Converts ascii
    :param source: Unicode string
    :return: Unicode string
    """
    return unicode(source) if isinstance(source, str) else source


def csv_to_list(source):
    """
    Returns python list from csv
    :param source: CSV string
    :return: List
    """
    return source.split(',') if (source is not None and source.has(',')) else source